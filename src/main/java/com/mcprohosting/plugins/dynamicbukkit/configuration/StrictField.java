package com.mcprohosting.plugins.dynamicbukkit.configuration;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface StrictField {

    public boolean isStrict() default true;
}
